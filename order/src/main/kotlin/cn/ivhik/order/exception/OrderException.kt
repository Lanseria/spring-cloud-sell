package cn.ivhik.product.exception

import cn.ivhik.order.enums.ResultEnum

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/9
 * Time: 19:48
 * To change this template use File | Settings | File and Code Templates.
 */
class OrderException : RuntimeException {

    private var code: Int? = null

    constructor(code: Int?, message: String) : super(message) {
        this.code = code
    }

    constructor(resultEnum: ResultEnum) : super(resultEnum.message) {
        this.code = resultEnum.code
    }
}