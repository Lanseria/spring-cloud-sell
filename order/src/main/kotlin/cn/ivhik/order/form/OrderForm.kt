package cn.ivhik.order.form

import org.hibernate.validator.constraints.NotEmpty

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/13
 * Time: 21:12
 * To change this template use File | Settings | File and Code Templates.
 */
data class OrderForm(
    /**
     * 买家姓名
     */
    @NotEmpty(message = "姓名必填")
    var name: String,
    /**
     * 买家手机号
     */
    @NotEmpty(message = "手机号必填")
    var phone: String,

    /**
     * 买家地址
     */
    @NotEmpty(message = "地址必填")
    var address: String,

    /**
     * 买家微信openid
     */
    @NotEmpty(message = "openid必填")
    var openid: String,

    /**
     * 购物车
     */
    @NotEmpty(message = "购物车不能为空")
    var items: String

)