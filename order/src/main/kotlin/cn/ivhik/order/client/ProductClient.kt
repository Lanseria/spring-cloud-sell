package cn.ivhik.order.client

import cn.ivhik.order.dataobject.ProductInfo
import cn.ivhik.order.dto.CartDto
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/9
 * Time: 14:44
 * To change this template use File | Settings | File and Code Templates.
 */
@FeignClient(name = "product")
interface ProductClient {
    @PostMapping("/product/listForOrder")
    fun listForOrder(@RequestBody productIds: List<String>): List<ProductInfo>

    @PostMapping("/product/decreaseStock")
    fun decreaseStock(@RequestBody cartDtoList: List<CartDto>)
}