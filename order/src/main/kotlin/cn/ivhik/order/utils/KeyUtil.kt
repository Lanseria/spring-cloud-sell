package cn.ivhik.order.utils

import java.lang.System.currentTimeMillis
import java.util.*

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/13
 * Time: 20:55
 * To change this template use File | Settings | File and Code Templates.
 */
object KeyUtil {

    /**
     * 生成唯一的主键
     * 格式: 时间+随机数
     */
    @Synchronized
    fun genUniqueKey(): String {
        val random = Random()
        val number = random.nextInt(900000) + 100000

        return currentTimeMillis().toString() + number.toString()
    }
}
