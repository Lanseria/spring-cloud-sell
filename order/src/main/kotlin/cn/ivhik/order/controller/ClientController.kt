package cn.ivhik.order.controller

import cn.ivhik.order.client.ProductClient
import cn.ivhik.order.dto.CartDto
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/9
 * Time: 13:42
 * To change this template use File | Settings | File and Code Templates.
 */
@RestController
class ClientController {
    @Autowired
    private lateinit var productClient: ProductClient

    @GetMapping("/getProductInfo")
    fun getProductInfoByIds(): String {
        val log = LoggerFactory.getLogger(this.javaClass)
        val res = productClient.listForOrder(listOf("164103465734242707"))
        log.info("response={}", res)
        return "ok"
    }

    @GetMapping("/productDecreaseStock")
    fun decreaseStock(): String {
        val cartDto = CartDto("157875196366160022", 1)
        productClient.decreaseStock(listOf(cartDto))
        return "ok"
    }
}