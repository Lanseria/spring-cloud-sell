package cn.ivhik.order.controller

import cn.ivhik.order.converter.OrderForm2OrderDTOConverter
import cn.ivhik.order.enums.ResultEnum
import cn.ivhik.order.form.OrderForm
import cn.ivhik.order.service.OrderService
import cn.ivhik.order.utils.ResultVoUtil
import cn.ivhik.order.vo.ResultVo
import cn.ivhik.product.exception.OrderException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.util.CollectionUtils
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid


/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/13
 * Time: 21:11
 * To change this template use File | Settings | File and Code Templates.
 */
@RestController
@RequestMapping("/order")
class OrderController {

    @Autowired
    private lateinit var orderService: OrderService

    /**
     * 1. 参数检验
     * 2. 查询商品信息(调用商品服务)
     * 3. 计算总价
     * 4. 扣库存(调用商品服务)
     * 5. 订单入库
     */

    @PostMapping("/create")
    fun create(
        @Valid orderForm: OrderForm,
        bindingResult: BindingResult
    ): ResultVo<Map<String, String>> {
        val log = LoggerFactory.getLogger(this.javaClass)
        if (bindingResult.hasErrors()) {
            log.error("【创建订单】参数不正确, orderForm={}", orderForm)
            throw OrderException(
                ResultEnum.PARAM_ERROR.code,
                bindingResult.fieldError!!.defaultMessage!!
            )
        }
        // orderForm -> orderDTO
        val orderDTO = OrderForm2OrderDTOConverter.convert(orderForm)
        if (CollectionUtils.isEmpty(orderDTO.orderDetailList)) {
            log.error("【创建订单】购物车信息为空")
            throw OrderException(ResultEnum.CART_EMPTY)
        }

        val result = orderService.create(orderDTO)

        val orderId = result.orderId
        val map = mapOf("orderId" to if (orderId != null) orderId else throw KotlinNullPointerException())
        return ResultVoUtil.success(map)
    }
}