package cn.ivhik.order.repository

import cn.ivhik.order.dataobject.OrderDetail
import org.springframework.data.jpa.repository.JpaRepository

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/8
 * Time: 21:17
 * To change this template use File | Settings | File and Code Templates.
 */
interface OrderDetailRepository : JpaRepository<OrderDetail, String> {
}