package cn.ivhik.order.repository

import cn.ivhik.order.dataobject.OrderMaster
import org.springframework.data.jpa.repository.JpaRepository

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/8
 * Time: 21:16
 * To change this template use File | Settings | File and Code Templates.
 */
interface OrderMasterRepository : JpaRepository<OrderMaster, String> {
}