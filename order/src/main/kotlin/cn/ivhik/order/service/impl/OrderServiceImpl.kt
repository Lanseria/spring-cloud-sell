package cn.ivhik.order.service.impl

import cn.ivhik.order.client.ProductClient
import cn.ivhik.order.dataobject.OrderDetail
import cn.ivhik.order.dataobject.OrderMaster
import cn.ivhik.order.dto.CartDto
import cn.ivhik.order.dto.OrderDto
import cn.ivhik.order.enums.OrderStatusEnum
import cn.ivhik.order.enums.PayStatusEnum
import cn.ivhik.order.repository.OrderDetailRepository
import cn.ivhik.order.repository.OrderMasterRepository
import cn.ivhik.order.service.OrderService
import cn.ivhik.order.utils.KeyUtil
import org.springframework.beans.BeanUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/13
 * Time: 20:42
 * To change this template use File | Settings | File and Code Templates.
 */
@Service
class OrderServiceImpl : OrderService {
    @Autowired
    private lateinit var orderDetailRepository: OrderDetailRepository

    @Autowired
    private lateinit var orderMasterRepository: OrderMasterRepository

    @Autowired
    private lateinit var productClient: ProductClient

    override fun create(orderDTO: OrderDto): OrderDto {

        val orderId = KeyUtil.genUniqueKey()

        //查询商品信息(调用商品服务)
        var orderDetailList: List<OrderDetail>? = orderDTO.orderDetailList
        val productIdList =
            (if (orderDetailList != null) orderDetailList else throw KotlinNullPointerException()).map { it.productId!! }
        val productInfoList = productClient.listForOrder(productIdList)

        //计算总价
        var orderAmount = BigDecimal.ZERO
        val orderDetailList1 = orderDTO.orderDetailList
        (if (orderDetailList1 != null) orderDetailList1 else throw KotlinNullPointerException()).forEach { orderDetail ->
            productInfoList.forEach { productInfo ->
                run {
                    if (productInfo.productId.equals(orderDetail.productId)) {
                        //单价*数量
                        orderAmount = productInfo.productPrice!!.multiply(BigDecimal(orderDetail.productQuantity!!))
                            .add(orderAmount)
                        BeanUtils.copyProperties(productInfo, orderDetail)
                        orderDetail.orderId = orderId
                        orderDetail.detailId = KeyUtil.genUniqueKey()
                        //订单详情入库
                        orderDetailRepository.save(orderDetail)
                    }
                }
            }
        }

        //扣库存(调用商品服务)
        val orderDetailList2 = orderDTO.orderDetailList
        val cartDtoList =
            (if (orderDetailList2 != null) orderDetailList2 else throw KotlinNullPointerException()).map { e ->
                CartDto(
                    e.productId!!,
                    e.productQuantity!!
                )
            }
        productClient.decreaseStock(cartDtoList)

        //订单入库
        val orderMaster = OrderMaster()
        orderDTO.orderId = orderId
        BeanUtils.copyProperties(orderDTO, orderMaster)
        orderMaster.orderId = orderId
        orderMaster.orderAmount = orderAmount
        orderMaster.orderStatus = OrderStatusEnum.NEW.code
        orderMaster.payStatus = PayStatusEnum.WAIT.code
        orderMasterRepository.save(orderMaster)
        return orderDTO
    }

}
