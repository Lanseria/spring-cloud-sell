package cn.ivhik.order.service

import cn.ivhik.order.dto.OrderDto

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/13
 * Time: 20:40
 * To change this template use File | Settings | File and Code Templates.
 */
interface OrderService {

    /**
     * 创建订单
     * @param orderDTO
     * @return
     */
    fun create(orderDTO: OrderDto): OrderDto
}
