package cn.ivhik.order.enums

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/8
 * Time: 21:34
 * To change this template use File | Settings | File and Code Templates.
 */
enum class OrderStatusEnum(val code: Int?, val message: String) {
    NEW(0, "新订单"),
    FINISHED(1, "完结"),
    CANCEL(2, "取消订单")
}