package cn.ivhik.order.enums

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/13
 * Time: 21:18
 * To change this template use File | Settings | File and Code Templates.
 */
enum class ResultEnum(val code: Int?, val message: String) {
    PARAM_ERROR(1, "参数错误"),
    CART_EMPTY(2, "购物车为空")
}