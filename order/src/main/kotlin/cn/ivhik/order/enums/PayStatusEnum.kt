package cn.ivhik.order.enums

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/8
 * Time: 21:37
 * To change this template use File | Settings | File and Code Templates.
 */
enum class PayStatusEnum(val code: Int?, val message: String) {
    WAIT(0, "等待支付"),
    SUCCESS(1, "支付成功")
}