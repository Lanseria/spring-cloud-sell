package cn.ivhik.order.converter

import cn.ivhik.order.dataobject.OrderDetail
import cn.ivhik.order.dto.OrderDto
import cn.ivhik.order.enums.ResultEnum
import cn.ivhik.order.form.OrderForm
import cn.ivhik.product.exception.OrderException
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken;
import org.slf4j.LoggerFactory


/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/13
 * Time: 21:24
 * To change this template use File | Settings | File and Code Templates.
 */
class OrderForm2OrderDTOConverter {
    companion object {
        fun convert(orderForm: OrderForm): OrderDto {
            val log = LoggerFactory.getLogger(this.javaClass)

            val orderDto = OrderDto()
            orderDto.buyerName = orderForm.name
            orderDto.buyerAddress = orderForm.address
            orderDto.buyerPhone = orderForm.phone
            orderDto.buyerOpenid = orderForm.openid

            val gson = Gson()

            val orderDetailList: List<OrderDetail>
            try {
                orderDetailList = gson.fromJson(
                    orderForm.items,
                    object : TypeToken<List<OrderDetail>>() {
                    }.type
                )
            } catch (e: Exception) {
                log.error("【json转换】错误, string={}", orderForm.items)
                throw OrderException(ResultEnum.PARAM_ERROR)
            }

            orderDto.orderDetailList = orderDetailList

            return orderDto
        }
    }

}