package cn.ivhik.order.dataobject

import java.math.BigDecimal
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/7
 * Time: 21:18
 * To change this template use File | Settings | File and Code Templates.
 */
@Entity
data class ProductInfo(
    @Id
    val productId: String? = null,

    /** 名字.  */
    val productName: String? = null,

    /** 单价.  */
    val productPrice: BigDecimal? = null,

    /** 库存.  */
    val productStock: Int? = null,

    /** 描述.  */
    val productDescription: String? = null,

    /** 小图.  */
    val productIcon: String? = null,

    /** 状态, 0正常1下架.  */
    val productStatus: Int? = null,

    /** 类目编号.  */
    val categoryType: Int? = null,

    val createTime: Date? = null,

    val updateTime: Date? = null
)