package cn.ivhik.order.dataobject

import java.math.BigDecimal
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id


/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/8
 * Time: 21:11
 * To change this template use File | Settings | File and Code Templates.
 */
@Entity
data class OrderMaster(

    /** 订单id.  */
    @Id
    var orderId: String? = null,

    /** 买家名字.  */
    var buyerName: String? = null,

    /** 买家手机号.  */
    var buyerPhone: String? = null,

    /** 买家地址.  */
    var buyerAddress: String? = null,

    /** 买家微信Openid.  */
    var buyerOpenid: String? = null,

    /** 订单总金额.  */
    var orderAmount: BigDecimal? = null,

    /** 订单状态, 默认为0新下单.  */
    var orderStatus: Int? = null,

    /** 支付状态, 默认为0未支付.  */
    var payStatus: Int? = null,

    /** 创建时间.  */
    var createTime: Date = Date(),

    /** 更新时间.  */
    var updateTime: Date = Date()
)