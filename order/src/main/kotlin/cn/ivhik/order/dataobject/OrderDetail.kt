package cn.ivhik.order.dataobject

import java.math.BigDecimal
import javax.persistence.Entity
import javax.persistence.Id


/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/8
 * Time: 21:11
 * To change this template use File | Settings | File and Code Templates.
 */
@Entity
data class OrderDetail(
    @Id
    var detailId: String? = null,

    /** 订单id.  */
    var orderId: String? = null,

    /** 商品id.  */
    var productId: String? = null,

    /** 商品名称.  */
    var productName: String? = null,

    /** 商品单价.  */
    var productPrice: BigDecimal? = null,

    /** 商品数量.  */
    var productQuantity: Int? = null,
    /** 商品小图.  */
    var productIcon: String? = null
)
