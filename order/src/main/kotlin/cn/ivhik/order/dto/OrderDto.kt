package cn.ivhik.order.dto

import cn.ivhik.order.dataobject.OrderDetail
import java.math.BigDecimal


/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/13
 * Time: 20:36
 * To change this template use File | Settings | File and Code Templates.
 */
data class OrderDto(
    var orderId: String? = null,
    var buyerName: String? = null,
    var buyerPhone: String? = null,
    var buyerAddress: String? = null,
    var buyerOpenid: String? = null,
    var orderAmount: BigDecimal? = null,
    var orderStatus: Int? = null,
    var payStatus: Int? = null,
    var orderDetailList: List<OrderDetail>? = null
)