package cn.ivhik.order.dto

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/9
 * Time: 19:38
 * To change this template use File | Settings | File and Code Templates.
 */
data class CartDto(
    var productId: String,
    var productQuantity: Int
)