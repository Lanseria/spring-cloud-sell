package cn.ivhik.order.repository

import cn.ivhik.order.OrderApplicationTests
import cn.ivhik.order.dataobject.OrderMaster
import cn.ivhik.order.enums.OrderStatusEnum
import cn.ivhik.order.enums.PayStatusEnum
import org.junit.Assert
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.math.BigDecimal

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/8
 * Time: 21:19
 * To change this template use File | Settings | File and Code Templates.
 */
@Component
class OrderMasterRepositoryTest : OrderApplicationTests() {
    @Autowired
    private lateinit var orderMasterRepository: OrderMasterRepository

    @Test
    fun testSave() {
        val orderMaster = OrderMaster()
        orderMaster.orderId = "213213123"
        orderMaster.buyerName = "师兄"
        orderMaster.buyerPhone = "2321313123"
        orderMaster.buyerAddress = "牛逼的舟山"
        orderMaster.buyerOpenid = "21312312"
        orderMaster.orderAmount = BigDecimal.valueOf(12.3)
        orderMaster.orderStatus = OrderStatusEnum.NEW.code
        orderMaster.payStatus = PayStatusEnum.WAIT.code
        val result = orderMasterRepository.save(orderMaster)
        Assert.assertTrue(result != null)
    }
}