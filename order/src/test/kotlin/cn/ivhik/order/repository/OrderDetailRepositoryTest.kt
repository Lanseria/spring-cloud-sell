package cn.ivhik.order.repository

import cn.ivhik.order.OrderApplicationTests
import cn.ivhik.order.dataobject.OrderDetail
import org.junit.Assert
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.math.BigDecimal

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/8
 * Time: 21:18
 * To change this template use File | Settings | File and Code Templates.
 */
@Component
class OrderDetailRepositoryTest : OrderApplicationTests() {
    @Autowired
    private lateinit var orderDetailRepository: OrderDetailRepository

    @Test
    fun testSave() {
        val orderDetail = OrderDetail()
        orderDetail.detailId = "21321321312"
        orderDetail.orderId = "23123213"
        orderDetail.productIcon = "./icon.jpg"
        orderDetail.productId = "157875196366160022"
        orderDetail.productName = "皮蛋粥"
        orderDetail.productPrice = BigDecimal.valueOf(0.01)
        orderDetail.productQuantity = 2
        val result = orderDetailRepository.save(orderDetail)
        Assert.assertTrue(result != null)
    }
}