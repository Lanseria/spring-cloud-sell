package cn.ivhik.product.repository

import cn.ivhik.product.ProductApplicationTests
import org.junit.Assert
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/7
 * Time: 21:37
 * To change this template use File | Settings | File and Code Templates.
 */
@Component
class ProductInfoRepositoryTest : ProductApplicationTests() {
    @Autowired
    private lateinit var productInfoRepository: ProductInfoRepository

    @Test
    @Throws(Exception::class)
    fun findByProductStatus() {
        val list = productInfoRepository.findByProductStatus(0)
        Assert.assertTrue(list.isNotEmpty())
    }

    @Test
    @Throws(Exception::class)
    fun findByProductIdIn() {
        val list = productInfoRepository.findByProductIdIn(listOf("157875196366160022", "157875227953464068"))
        Assert.assertTrue(list.isNotEmpty())
    }
}