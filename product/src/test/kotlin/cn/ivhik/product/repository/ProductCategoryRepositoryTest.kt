package cn.ivhik.product.repository

import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/7
 * Time: 22:06
 * To change this template use File | Settings | File and Code Templates.
 */
@Component
class ProductCategoryRepositoryTest : ProductInfoRepositoryTest() {
    @Autowired
    private val productCategoryRepository: ProductCategoryRepository? = null

    @Test
    @Throws(Exception::class)
    fun productCategoryRepository() {
        productCategoryRepository!!.findByCategoryTypeIn(listOf(11, 22))
    }
}