package cn.ivhik.product.service

import cn.ivhik.product.ProductApplicationTests
import org.junit.Assert
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.*

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/7
 * Time: 22:44
 * To change this template use File | Settings | File and Code Templates.
 */
@Component
class CategoryServiceTest : ProductApplicationTests() {
    @Autowired
    internal var categoryService: CategoryService? = null

    @Test
    fun findByCategoryTypeIn() {
        val list = categoryService!!.findByCategoryTypeIn(Arrays.asList(11, 22))
        Assert.assertTrue(list.isNotEmpty())
    }
}