package cn.ivhik.product.service

import cn.ivhik.product.ProductApplicationTests
import cn.ivhik.product.dto.CartDto
import org.junit.Assert
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/7
 * Time: 22:42
 * To change this template use File | Settings | File and Code Templates.
 */
@Component
class ProductServiceTest : ProductApplicationTests() {

    @Autowired
    private lateinit var productService: ProductService

    @Test
    @Throws(Exception::class)
    fun findUpAll() {
        val list = productService.findUpAll()
        Assert.assertTrue(list.isNotEmpty())
    }

    @Test
    @Throws(Exception::class)
    fun findListByIds() {
        val productIds = listOf<String>("157875196366160022", "157875227953464068")
        val list = productService.findListByIds(productIds)
        Assert.assertTrue(list.isNotEmpty())
    }

    @Test
    @Throws(Exception::class)
    fun decreaseStock() {
        val cartDto = CartDto("157875196366160022", 1)
        productService.decreaseStock(listOf(cartDto))
    }
}