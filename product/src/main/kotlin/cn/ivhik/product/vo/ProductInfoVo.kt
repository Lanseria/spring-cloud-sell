package cn.ivhik.product.vo

import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/8
 * Time: 20:21
 * To change this template use File | Settings | File and Code Templates.
 */
data class ProductInfoVo(
    @JsonProperty("id")
    var productId: String? = null,

    @JsonProperty("name")
    var productName: String? = null,

    @JsonProperty("price")
    var productPrice: BigDecimal? = null,

    @JsonProperty("description")
    var productDescription: String? = null,

    @JsonProperty("icon")
    var productIcon: String? = null
)