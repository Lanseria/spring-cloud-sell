package cn.ivhik.product.vo

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/8
 * Time: 20:20
 * To change this template use File | Settings | File and Code Templates.
 */
data class ProductVo(
    @JsonProperty("name")
    var categoryName: String? = null,

    @JsonProperty("type")
    var categoryType: Int? = null,

    @JsonProperty("foods")
    var productInfoVOList: List<ProductInfoVo>? = null
)