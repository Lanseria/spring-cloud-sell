package cn.ivhik.product.vo

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/7
 * Time: 22:21
 * To change this template use File | Settings | File and Code Templates.
 */
data class ResultVo<T>(
    /** 错误码  */
    var code: Int = 0,
    /** 提示信息  */
    var msg: String = "Success",
    /** 具体内容  */
    var data: T? = null
)