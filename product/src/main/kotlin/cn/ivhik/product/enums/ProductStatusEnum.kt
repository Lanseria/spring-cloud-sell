package cn.ivhik.product.enums

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/7
 * Time: 22:15
 * To change this template use File | Settings | File and Code Templates.
 */
enum class ProductStatusEnum(val code: Int?, val message: String) {
    UP(0, "在架"),
    DOWN(1, "下架")
}
