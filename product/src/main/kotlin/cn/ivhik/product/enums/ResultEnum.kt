package cn.ivhik.product.enums

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/9
 * Time: 19:50
 * To change this template use File | Settings | File and Code Templates.
 */
enum class ResultEnum(var code: Int, var msg: String) {
    PRODUCT_NOT_EXIST(1, "商品不存在"),
    PRODUCT_STOCK_ERROR(2, "库存错误")
}