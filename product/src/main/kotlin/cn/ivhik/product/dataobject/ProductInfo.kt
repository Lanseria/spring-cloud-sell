package cn.ivhik.product.dataobject

import java.math.BigDecimal
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/7
 * Time: 21:18
 * To change this template use File | Settings | File and Code Templates.
 */
@Entity
data class ProductInfo(
    @Id
    var productId: String? = null,

    /** 名字.  */
    var productName: String? = null,

    /** 单价.  */
    var productPrice: BigDecimal? = null,

    /** 库存.  */
    var productStock: Int = 0,

    /** 描述.  */
    var productDescription: String? = null,

    /** 小图.  */
    var productIcon: String? = null,

    /** 状态, 0正常1下架.  */
    var productStatus: Int? = null,

    /** 类目编号.  */
    var categoryType: Int? = null,

    var createTime: Date? = null,

    var updateTime: Date? = null
)