package cn.ivhik.product.dataobject

import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/7
 * Time: 21:28
 * To change this template use File | Settings | File and Code Templates.
 */
@Entity
data class ProductCategory(
    @Id
    @GeneratedValue
    val categoryId: Int? = null,

    /**
     * 类目名字.
     */
    val categoryName: String? = null,

    /**
     * 类目编号.
     */
    val categoryType: Int? = null,

    val createTime: Date? = null,

    val updateTime: Date? = null
)