package cn.ivhik.product.controller

import cn.ivhik.product.dataobject.ProductInfo
import cn.ivhik.product.dto.CartDto
import cn.ivhik.product.service.CategoryService
import cn.ivhik.product.service.ProductService
import cn.ivhik.product.utils.ResultVoUtil
import cn.ivhik.product.vo.ProductInfoVo
import cn.ivhik.product.vo.ProductVo
import cn.ivhik.product.vo.ResultVo
import org.springframework.beans.BeanUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/7
 * Time: 22:37
 * To change this template use File | Settings | File and Code Templates.
 */
@RestController
@RequestMapping("/product")
class ProductController {
    @Autowired
    private lateinit var productService: ProductService
    @Autowired
    private lateinit var categoryService: CategoryService

    /** 1. 查询所有在架商品 2. 获取类目type列表 3. 查询类目 4. 构造数据  */
    @GetMapping("/list")
    fun list(): ResultVo<ArrayList<ProductVo>> {
        val productInfoList = productService.findUpAll()
        val categoryTypeList = productInfoList.map { it.categoryType!! }
        val categoryList = categoryService.findByCategoryTypeIn(categoryTypeList)
        val productVOList = ArrayList<ProductVo>()
        for (productCategory in categoryList) {
            val productVO = ProductVo()
            productVO.categoryName = productCategory.categoryName
            productVO.categoryType = productCategory.categoryType
            val productInfoVOList = ArrayList<ProductInfoVo>()
            for (productInfo in productInfoList) {
                if (productInfo.categoryType!! == productCategory.categoryType) {
                    val productInfoVO = ProductInfoVo()
                    BeanUtils.copyProperties(productInfo, productInfoVO)
                    productInfoVOList.add(productInfoVO)
                }
            }
            productVO.productInfoVOList = productInfoVOList
            productVOList.add(productVO)
        }
        return ResultVoUtil.success(productVOList)
    }

    /**
     * 获取商品列表(给订单服务用的)
     */
    @PostMapping("/listForOrder")
    fun listForOrder(@RequestBody productIds: List<String>): List<ProductInfo> {
        return productService.findListByIds(productIds)
    }

    @PostMapping("/decreaseStock")
    fun decreaseStock(@RequestBody cartDtoList: List<CartDto>) {
        productService.decreaseStock(cartDtoList)
    }

}