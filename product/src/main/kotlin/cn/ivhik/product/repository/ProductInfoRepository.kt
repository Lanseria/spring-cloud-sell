package cn.ivhik.product.repository

import cn.ivhik.product.dataobject.ProductInfo
import org.springframework.data.jpa.repository.JpaRepository

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/7
 * Time: 21:29
 * To change this template use File | Settings | File and Code Templates.
 */
interface ProductInfoRepository : JpaRepository<ProductInfo, String> {
    fun findByProductStatus(productStatus: Int?): List<ProductInfo>
    fun findByProductIdIn(productIds: List<String>): List<ProductInfo>
}