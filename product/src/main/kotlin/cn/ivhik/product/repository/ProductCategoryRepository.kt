package cn.ivhik.product.repository

import cn.ivhik.product.dataobject.ProductCategory
import org.springframework.data.jpa.repository.JpaRepository

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/7
 * Time: 22:04
 * To change this template use File | Settings | File and Code Templates.
 */
interface ProductCategoryRepository : JpaRepository<ProductCategory, Int> {
    fun findByCategoryTypeIn(categoryTypeList: List<Int>): List<ProductCategory>
}