package cn.ivhik.product.utils

import cn.ivhik.product.vo.ResultVo

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/7
 * Time: 22:21
 * To change this template use File | Settings | File and Code Templates.
 */
class ResultVoUtil {
    companion object {
        fun <T> success(objects: T): ResultVo<T> {
            val resultVo = ResultVo<T>()
            resultVo.data = objects
            resultVo.code = 0
            resultVo.msg = "成功"
            return resultVo
        }
    }
}