package cn.ivhik.product.service

import cn.ivhik.product.dataobject.ProductInfo
import cn.ivhik.product.dto.CartDto
import org.springframework.stereotype.Service

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/7
 * Time: 22:39
 * To change this template use File | Settings | File and Code Templates.
 */
@Service
interface ProductService {
    /** 查询所有在架商品列表  */
    fun findUpAll(): List<ProductInfo>

    /**
     * 查询所有商品信息通过productId
     */
    fun findListByIds(productIds: List<String>): List<ProductInfo>

    /**
     * 扣库存
     */
    fun decreaseStock(cartDtoList: List<CartDto>)
}