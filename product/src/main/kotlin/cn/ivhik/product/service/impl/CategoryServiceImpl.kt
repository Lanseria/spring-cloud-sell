package cn.ivhik.product.service.impl

import cn.ivhik.product.dataobject.ProductCategory
import cn.ivhik.product.repository.ProductCategoryRepository
import cn.ivhik.product.service.CategoryService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/8
 * Time: 20:16
 * To change this template use File | Settings | File and Code Templates.
 */
@Service
class CategoryServiceImpl @Autowired
constructor(private val productCategoryRepository: ProductCategoryRepository) : CategoryService {

    override fun findByCategoryTypeIn(categoryTypeList: List<Int>): List<ProductCategory> {
        return productCategoryRepository.findByCategoryTypeIn(categoryTypeList)
    }
}