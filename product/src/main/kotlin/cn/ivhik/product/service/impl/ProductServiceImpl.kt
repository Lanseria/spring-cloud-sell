package cn.ivhik.product.service.impl

import cn.ivhik.product.dataobject.ProductInfo
import cn.ivhik.product.dto.CartDto
import cn.ivhik.product.enums.ProductStatusEnum
import cn.ivhik.product.enums.ResultEnum
import cn.ivhik.product.exception.ProductException
import cn.ivhik.product.repository.ProductInfoRepository
import cn.ivhik.product.service.ProductService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/8
 * Time: 20:03
 * To change this template use File | Settings | File and Code Templates.
 */
@Service
class ProductServiceImpl @Autowired
constructor(internal val productInfoRepository: ProductInfoRepository) : ProductService {

    override fun decreaseStock(cartDtoList: List<CartDto>) {
        cartDtoList.forEach {
            val productInfoOptional = productInfoRepository.findById(it.productId)
            // 判断商品是否存在
            if (!productInfoOptional.isPresent) {
                throw ProductException(ResultEnum.PRODUCT_NOT_EXIST)
            }
            val productInfo = productInfoOptional.get()
            // 库存是否足够
            val result = productInfo.productStock - it.productQuantity
            if (result < 0) {
                throw ProductException(ResultEnum.PRODUCT_STOCK_ERROR)
            }
            productInfo.productStock = result
            productInfoRepository.save(productInfo)
        }
    }

    override fun findListByIds(productIds: List<String>): List<ProductInfo> {
        return productInfoRepository.findByProductIdIn(productIds)
    }

    override fun findUpAll(): List<ProductInfo> {
        return productInfoRepository.findByProductStatus(ProductStatusEnum.UP.code)
    }
}
