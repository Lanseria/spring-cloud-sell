package cn.ivhik.product.service

import cn.ivhik.product.dataobject.ProductCategory
import org.springframework.stereotype.Service

/**
 * Created by spring-cloud-sell.
 * User: lanseria
 * Date: 2019/2/7
 * Time: 22:45
 * To change this template use File | Settings | File and Code Templates.
 */
@Service
interface CategoryService {
    fun findByCategoryTypeIn(categoryTypeList: List<Int>): List<ProductCategory>
}